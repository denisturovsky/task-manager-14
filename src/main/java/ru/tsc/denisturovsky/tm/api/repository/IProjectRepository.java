package ru.tsc.denisturovsky.tm.api.repository;

import ru.tsc.denisturovsky.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    boolean existsById(String id);

    Project remove(Project project);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project task);

    void clear();

}
