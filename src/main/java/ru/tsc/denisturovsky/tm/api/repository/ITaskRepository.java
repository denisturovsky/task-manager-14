package ru.tsc.denisturovsky.tm.api.repository;

import ru.tsc.denisturovsky.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    void clear();

}
