package ru.tsc.denisturovsky.tm.enumerated;

import ru.tsc.denisturovsky.tm.comparator.CreatedComparator;
import ru.tsc.denisturovsky.tm.comparator.DateBeginComparator;
import ru.tsc.denisturovsky.tm.comparator.NameComparator;
import ru.tsc.denisturovsky.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by Name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_DATE_BEGIN("Sort by date begin", DateBeginComparator.INSTANCE);

    private String displayName;

    private final Comparator comparator;

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Sort sort: values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
