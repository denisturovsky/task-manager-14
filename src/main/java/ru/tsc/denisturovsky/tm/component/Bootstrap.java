package ru.tsc.denisturovsky.tm.component;

import ru.tsc.denisturovsky.tm.api.controller.ICommandController;
import ru.tsc.denisturovsky.tm.api.controller.IProjectController;
import ru.tsc.denisturovsky.tm.api.controller.IProjectTaskController;
import ru.tsc.denisturovsky.tm.api.controller.ITaskController;
import ru.tsc.denisturovsky.tm.api.repository.ICommandRepository;
import ru.tsc.denisturovsky.tm.api.repository.IProjectRepository;
import ru.tsc.denisturovsky.tm.api.repository.ITaskRepository;
import ru.tsc.denisturovsky.tm.api.service.ICommandService;
import ru.tsc.denisturovsky.tm.api.service.IProjectService;
import ru.tsc.denisturovsky.tm.api.service.IProjectTaskService;
import ru.tsc.denisturovsky.tm.api.service.ITaskService;
import ru.tsc.denisturovsky.tm.constant.TerminalArgument;
import ru.tsc.denisturovsky.tm.constant.TerminalCommand;
import ru.tsc.denisturovsky.tm.controller.CommandController;
import ru.tsc.denisturovsky.tm.controller.ProjectController;
import ru.tsc.denisturovsky.tm.controller.ProjectTaskController;
import ru.tsc.denisturovsky.tm.controller.TaskController;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.model.Task;
import ru.tsc.denisturovsky.tm.repository.CommandRepository;
import ru.tsc.denisturovsky.tm.repository.ProjectRepository;
import ru.tsc.denisturovsky.tm.repository.TaskRepository;
import ru.tsc.denisturovsky.tm.service.CommandService;
import ru.tsc.denisturovsky.tm.service.ProjectService;
import ru.tsc.denisturovsky.tm.service.ProjectTaskService;
import ru.tsc.denisturovsky.tm.service.TaskService;
import ru.tsc.denisturovsky.tm.util.DateUtil;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void run(final String[] args) {
        if (processArgument(args)) System.exit(0);
        Scanner scanner = new Scanner(System.in);
        initData();
        commandController.showWelcome();
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private void initData() {
        taskService.add(new Task("TASK TEST", Status.IN_PROGRESS, DateUtil.toDate("10.05.2018")));
        taskService.add(new Task("TASK BETA", Status.NOT_STARTED, DateUtil.toDate("05.11.2020")));
        taskService.add(new Task("TASK BEST", Status.IN_PROGRESS, DateUtil.toDate("15.10.2019")));
        taskService.add(new Task("TASK MEGA", Status.COMPLETED, DateUtil.toDate("07.03.2021")));

        projectService.add(new Project("PROJECT DEMO", Status.IN_PROGRESS, DateUtil.toDate("07.05.2018")));
        projectService.add(new Project("PROJECT TEST", Status.NOT_STARTED, DateUtil.toDate("03.11.2020")));
        projectService.add(new Project("PROJECT BEST", Status.IN_PROGRESS, DateUtil.toDate("05.10.2019")));
        projectService.add(new Project("PROJECT MEGA", Status.COMPLETED, DateUtil.toDate("04.03.2021")));
    }

    public void close() {
        System.exit(0);
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalArgument.ABOUT:
                commandController.showAbout();
                break;
            case TerminalArgument.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalArgument.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalArgument.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalArgument.HELP:
                commandController.showHelp();
                break;
            case TerminalArgument.VERSION:
                commandController.showVersion();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalCommand.ABOUT:
                commandController.showAbout();
                break;
            case TerminalCommand.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalCommand.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalCommand.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalCommand.HELP:
                commandController.showHelp();
                break;
            case TerminalCommand.VERSION:
                commandController.showVersion();
                break;
            case TerminalCommand.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalCommand.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalCommand.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalCommand.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalCommand.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalCommand.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalCommand.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalCommand.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalCommand.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalCommand.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalCommand.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalCommand.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalCommand.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalCommand.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalCommand.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalCommand.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalCommand.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalCommand.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskListByProjectId();
                break;
            case TerminalCommand.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalCommand.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalCommand.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalCommand.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalCommand.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalCommand.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalCommand.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalCommand.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalCommand.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalCommand.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalCommand.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalCommand.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalCommand.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalCommand.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalCommand.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalCommand.EXIT:
                close();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

}
